FROM alpine:3.13 AS builder
RUN apk add --no-cache meson ldc ldc-runtime appstream-dev libarchive-dev \
	gir-to-d cairo-dev gdk-pixbuf-dev librsvg-dev freetype-dev fontconfig-dev \
	pango-dev yarn lmdb-dev glibd-dev libsoup-dev gobject-introspection-dev \
	coreutils git alpine-sdk
RUN git clone https://github.com/Cogitri/appstream-generator -b alpine-build
WORKDIR /appstream-generator
RUN meson --buildtype=debug build && ninja -C build install

FROM alpine:3.13
RUN apk add --no-cache appstream libarchive cairo ldc-runtime fontconfig freetype pango \
	gdk-pixbuf glibd gettext lmdb libsoup librsvg optipng ffmpeg
COPY --from=builder /usr/local/bin/appstream-generator /usr/local/bin/appstream-generator
COPY --from=builder /usr/local/share/appstream/ /usr/local/share/appstream/

WORKDIR /cache/export
COPY cron /etc/periodic
